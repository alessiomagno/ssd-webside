﻿namespace WebServerAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data;
    using System.Data.SqlClient;
    using System.Configuration;
    using System.Data.Common;
    using WebServerAPI.Models;
    using System.Data.SQLite;

    namespace WebServerApi
    {
        class Persist
        {
            public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento (mittente e testo generato per l'interfaccia)
            public event viewEventHandler FlushText;  // questo genera l'evento
            public void doSomething()
            {
                for (int i = 0; i < 10; i++)
                    FlushText(this, $"i={i}");
            }

            public void ConnectWithFactory(string connString, bool isSQLLite, string idCliente)

            {
                DataSet ds = new DataSet();
                DbProviderFactory dbFactory = DbProviderFactories.GetFactory("System.Data.SQLite");
                using (DbConnection conn = dbFactory.CreateConnection())
                {
                    try
                    {
                        conn.ConnectionString = connString;
                        conn.Open();
                        DbDataAdapter dbAdapter = dbFactory.CreateDataAdapter();
                        DbCommand dbCommand = conn.CreateCommand();
                        dbCommand.CommandText = "SELECT idserie,periodo,val FROM histordini where idserie = @id";
                        IDbDataParameter param = dbCommand.CreateParameter();
                        param.DbType = DbType.Int32;
                        param.ParameterName = "@id";
                        param.Value = Convert.ToInt32(idCliente);
                        dbCommand.Parameters.Add(param);
                        dbAdapter.SelectCommand = dbCommand;
                        dbAdapter.Fill(ds);
                        ds.Tables[0].TableName = "histordini";
                        foreach (DataRow dr in ds.Tables["histordini"].Rows)
                            FlushText(this, dr["id"] + " " + dr["periodo"] + dr["val"]);
                    }
                    catch (Exception ex)
                    {
                        FlushText(this, "[FillDataSet] Error: " + ex.Message);
                    }
                    finally
                    {
                        if (conn.State == ConnectionState.Open) conn.Close();
                    }


                }
            }

            public void Connect(string connString, bool isSQLLite, string idCliente)
            {

                IDbConnection conn = null;
                try
                {
                    if (isSQLLite) conn = new SQLiteConnection(connString);
                    else conn = new SqlConnection(connString);

                    conn.Open();
                    IDbCommand com = conn.CreateCommand();
                    //string queryText = "select id,nome from clienti";
                    string queryText = "SELECT idserie,periodo,val FROM histordini where idserie = @id";
                    com.CommandText = queryText;

                    IDbDataParameter param = com.CreateParameter();
                    param.DbType = DbType.Int32;
                    param.ParameterName = "@id";
                    param.Value = Convert.ToInt32(idCliente);
                    com.Parameters.Add(param);

                    IDataReader reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        FlushText(this, reader["idserie"] + " " + reader["periodo"] + " " + reader["val"]);
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    FlushText(this, e.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }

            }

            public int execNonQueryViaF(string connString, string queryText, string factory)
            {
                int numRow = 0;
                DbProviderFactory dbFactory = null;

                dbFactory = DbProviderFactories.GetFactory(factory);

                using (DbConnection conn = dbFactory.CreateConnection())
                {
                    try
                    {
                        conn.ConnectionString = connString;
                        conn.Open();
                        IDbCommand com = conn.CreateCommand();
                        com.CommandText = queryText;
                        numRow = com.ExecuteNonQuery();

                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        FlushText(this, ex.Message);
                    }
                    finally
                    {
                        if (conn.State == ConnectionState.Open)
                            conn.Close();
                    }
                }
                return numRow;
            }

            public string readTableViaF(string connString, string queryText, string factory)
            {
                int i, numcol;
                string res = "[";
                List<string> columns = new List<string>();
                DbProviderFactory dbFactory = null;

                dbFactory = DbProviderFactories.GetFactory(factory);

                using (DbConnection conn = dbFactory.CreateConnection())
                {
                    try
                    {
                        conn.ConnectionString = connString;
                        conn.Open();
                        IDbCommand com = conn.CreateCommand();
                        com.CommandText = queryText;
                        IDataReader reader = com.ExecuteReader();

                        numcol = reader.FieldCount;
                        for (i = 0; i < numcol; i++)
                            columns.Add(reader.GetName(i));

                        while (reader.Read())
                        {
                            res += "{";
                            for (i = 0; i < numcol; i++)
                            {
                                res += "\"" + columns[i] + "\":\"" + reader[i] + "\",";
                            }
                            res += "},";
                            res = res.Replace(",}", "}");
                        }
                        reader.Close();
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        FlushText(this, ex.Message);
                    }
                    finally
                    {
                        if (conn.State == ConnectionState.Open)
                            conn.Close();
                    }
                }
                return (res + "]").Replace(",]", "]");
            
            }

        }
    }


}