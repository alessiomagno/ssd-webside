﻿var n;   // num clienti
var m;   // num server (facilities)
var c;   // matrice dei costi
var req; // matrice delle richieste
var cap; // vattore delle capacità
var sol; // vettore soluzione (contiene gli indici dei magazzini a cui ha assegnato i client)
var solbest; // vettore migliore soluzione trovata
var startTime, endTime, timeDiff; // tempi esecuzione
var zub = Number.MAX_VALUE;       // costo miglior soluzione trovata
var zlbBest = Number.MAX_VALUE;   // best lower bound

var jInstance;    // istanza in input
var EPS = 0.001;