﻿function constructive() {

    var dist = new Array(m);
    var capLeft = new Array(m);
    sol = new Array(m);
    zub = 0;

    for (i = 0; i < m; i++) {
        dist[i] = new Array(2);
        capLeft[i] = cap[i]
    }

    for (j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            dist[i][0] = req[i][j];
            dist[i][1] = i;
        }

        dist.sort(function (x, y) { return x - y });

        for (ii = 0; ii < m; ii++) {
            i = dist[ii][1];
            if (capLeft[i] >= req[i][j]) {
                capLeft[i] -= req[i][j]
                sol[j] = i
                zub += c[i][j]
                break;
            }
        }
    }
    if (checkSol(sol) == zub) alert("Soluzione ammissibile! Final Zub Constructive=" + zub)
    else alert("Soluzione non ammissibile!")
}

function runOpt10() {
    //constructive();
    var cost = c;
    var z = lSearchOpt10(cost);
    var zcheck = checkSol(sol);
    if (z == zcheck) alert("Soluzione ammissibile! Final Zub Opt10 =" + z)
    else alert("Soluzione non ammissibile!")
}

function lSearchOpt10(cost) {

    var i, j = 0;
    var isImproved;
    var z = 0;
    var capLeft = cap.slice();

    for (var j = 0; j < n; j++) {
        capLeft[sol[j]] -= req[sol[j]][j];
        z += cost[sol[j]][j];
        console.log("temp z : " + z);

    }

    do {
        isImproved = false;
        for (j = 0; j < n; j++) {
            for (i = 0; i < m; i++) { 
                if(cost[i][j] < cost[sol[j]][j] && capLeft[i] >= req[i][j]){
                    capLeft[sol[j]] += req[sol[j]][j];
                    capLeft[i] -= req[i][j];
                    z -= cost[sol[j]][j];

                    sol[j] = i;
                    z += cost[sol[j]][j];

                    console.log("opt10, improvement. z=" + z);
                    isImproved = true;
                    break;
                }
            }
            if (isImproved) break;
        }
    } while (isImproved);
     return z;

}