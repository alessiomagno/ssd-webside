﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServerAPI.Models;
using System.Web.Http;
using System.Configuration;
using Newtonsoft.Json;
using WebServerAPI.Controllers.WebServerApi;

namespace WebServerAPI.Controllers
{
    public class ClientiController : ApiController
    {

        public class Cli
        {
            public int id;
            public int mag;
            public int req;
        }
        public class Mag
        {
            public int id;
            public int cap;
            public int req;
        }

        public class Cost
        {
            public int id;
            public int mag;
            public int cli;
            public int cost;
        }

        public class Instance
        {
            public string name;
            public int numcustomers;
            public int numfacilities;
            public int[,] cost;
            public int[,] req;
            public int[] cap;
        }

        [HttpGet]                // in esecuzione solo con un get dal client
        [ActionName("GetAllClients")]   // nome del metodo esposto nella API
        public string GetAllClient()
        {

            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            Persist P = new Persist();
            string queryText = "select id,req,mag FROM clienti";
            return JsonConvert.SerializeObject(P.readTableViaF(connString, queryText, factory));
        }

        [HttpGet] // in esecuzione solo con un get dal client
        [ActionName("GetClientById")] // nome del metodo esposto
        public string GetClientById(int id)
        {
            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            Persist P = new Persist();
            string queryText = "select id,req,mag FROM clienti where id =" + id;
            return JsonConvert.SerializeObject(P.readTableViaF(connString, queryText, factory));
        }

        [HttpPost]
        [ActionName("insertCustomer")]
        public string InsertCustomer(Clienti obj)
        {
            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            string queryString = "insert into clienti (id, req, mag) values(";
            queryString += obj.id + "," + obj.req + ",'" + obj.mag + "')";
            Persist P = new Persist();
            P.execNonQueryViaF(connString, queryString, factory);
            return "Customer inserted"; // oppure dichiararla static
        }

        [HttpPost]
        [ActionName("updateCustomer")]
        public string UpdateCustomer(Clienti obj)
        {
            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            string queryString = "update clienti set req = " + obj.req + " where id=" + obj.id;
            Persist P = new Persist();
            P.execNonQueryViaF(connString, queryString, factory);
            return "Customer updated"; // oppure dichiararla static
        }

        [HttpDelete]
        [ActionName("deleteCustomer")]
        public string deleteCustomer(int id)
        {
            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            Persist P = new Persist();
            string queryString = "delete from clienti where id=" + id;
            P.execNonQueryViaF(connString, queryString, factory);
            return "Customer deleted";
        }

        public string getGAPInstance(int id) 
        {
            string factory = "System.Data.SQLite";
            string connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            Persist P = new Persist();
            string res;
            int numcustomers,numfacilities;
            Instance instance = new Instance();
            instance.name = "fromDB";

            

            string queryTextMag = "select * from magazzini";
            string sm = P.readTableViaF(connString, queryTextMag, factory);
            Mag[] mag = JsonConvert.DeserializeObject<Mag[]>(sm);
            numfacilities = mag.Length;
            instance.numfacilities = numfacilities;

            string queryTextCli = "select * from clienti";
            string sc = P.readTableViaF(connString, queryTextCli, factory);
            Cli[] cli = JsonConvert.DeserializeObject<Cli[]>(sc);
            numcustomers = cli.Length;
            instance.numcustomers = numcustomers / numfacilities;


            string queryTextCost = "select * from costi";
            string scs= P.readTableViaF(connString, queryTextCost, factory);
            Cost[] cost = JsonConvert.DeserializeObject<Cost[]>(scs);

            int[,] costs = new int[instance.numfacilities, instance.numcustomers];
            for (int i = 0; i < cost.Length; i++)
            {
                costs[cost[i].mag - 1, cost[i].cli - 1] = cost[i].cost;
            }
            instance.cost = costs;

            int[,] reqs = new int[instance.numfacilities, instance.numcustomers];
            for (int i = 0; i < cli.Length; i++)
            {
                reqs[cli[i].mag - 1, cli[i].id - 1] = cli[i].req;
            }
            instance.req = reqs;

            int[] caps = new int[instance.numfacilities];
            for (int i = 0; i < mag.Length; i++)
            {
                caps[mag[i].id - 1] = mag[i].cap;
            }
            instance.cap = caps;

            res = JsonConvert.SerializeObject(instance);
            
            return res;
        }

    }

}